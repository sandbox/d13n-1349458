-- SUMMARY --

The Menu MultiCol module creates and maintains a 3 level menu hierarchy of
menu links for nodes of a specified type, thus to provide a multi column
menu structure that can be used by modules like menu_mega and such.

For a full description of the module, visit the project page:
  http://drupal.org/project/menu_multicol

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/menu_multicol

-- REQUIREMENTS --

 - Menu module for altering the menu structure
 - Special menu items to provide unlinked, hidden column menus


-- INSTALLATION --

* Install as usual, see http://drupal.org/documentation/install/modules-themes/modules-7 for further information.

* You likely want to set up menu_mega or similar to actually display the menu in columns.


-- CONFIGURATION --

* Configure user permissions in Administration -> People -> Permissions:

  - Administer menus and menu items

    Users in roles with the "Administer menus and menu items" permission will see
    the configuration under Structure -> Menus -> Multi Column Menu.


-- CONTACT --

Current maintainers:
* Dietmar Ludmann (d13n) - http://drupal.org/user/134134

This project has been sponsored by:
* VALORA EFFEKTEN HANDEL AG
  Specialized in german otc shares, the VEH AG also powers the OeKOPORTAL
  which can be found at: http://oekoportal.de/.
  Also visit http://valora.de/ for more information.
