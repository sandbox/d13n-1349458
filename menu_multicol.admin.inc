<?php

/**
  * @file
  * Administration for multi column menu structures.
  */

/**
 * mcms list
 * add a new multicol menu or edit the saved ones
 *
 * @ingroup pages
 */
function mcms_list() {
  $title = t('Multi Column Menus');
  $description = t('List of already saved menu structures.');
  $output = '<h4>' . $title . '</h4>';
  $output .= '<p>' . $description . '</p>';

  $addnew = t('Add new menu structure');
  $output .= '<p>' . l('+ ' . $addnew . '.', 'admin/structure/menu/multicol/add') . '</p>';

  $variables = array();
  $variables['header'] = array(
//    'mlid' => array(
//      'data' => t('Menu Link ID'),
//      'field' => 'mlid',
//      'sort' => 'asc',
//    ),
    'name' => array(
      'data' => t('Name'),
      'field' => 'name',
      'sort' => 'asc',
    ),
    'colcount' => array(
      'data' => t('Column Count'),
      'field' => 'colcount',
      'sort' => 'asc',
    ),
    'colprefix' => array(
      'data' => t('Column Prefix'),
      'field' => 'colprefix',
      'sort' => 'asc',
    ),
    'colsort' => array(
      'data' => t('Sort Order'),
      'field' => 'colsort',
      'sort' => 'asc',
    ),
    'type' => array(
      'data' => t('Node Type'),
      'field' => 'type',
      'sort' => 'asc',
    ),
    'linktitle' => array(
      'data' => t('Link Title'),
      'field' => 'linktitle',
      'sort' => 'asc',
    ),
    'autoupdate' => array(
      'data' => t('Auto Update'),
      'field' => 'autoupdate',
      'sort' => 'asc',
    ),
  );

  $variables['rows'] = array();
  $result = db_query('SELECT * FROM {menu_multicol}');
  foreach ($result as $row) {
    $variables['rows'][] = array(
//      $row->mlid,
      l($row->name, 'admin/structure/menu/multicol/' . $row->mlid),
      $row->colcount,
      $row->colprefix,
      $row->colsort ? t('horizontal') : t('vertical'),
      $row->type,
      $row->linktitle,
      $row->autoupdate,
    );
  }

  $variables['attributes'] = array();
  $variables['caption'] = t('menu structure table');
  $variables['colgroups'] = array();
  $variables['sticky'] = TRUE;
  $variables['empty'] = t('No menu structures are in database. Use "!addnew" to create one.',
    array('!addnew' => l($addnew, 'admin/structure/menu/multicol/add')));
  //dvm($variables);
  $output .= theme_table($variables);

  if (count($variables['rows']) > 0) {
    $output .= t('HINT: Click on the name to edit an existing menu structure.');
  }

  return $output;
} // mcms_list()

/**
 * mcms form
 * add / create / clear a multi column menu structure
 *
 * @ingroup forms
 * @param variant mlid menu link id of the entry to be edited, or 'add' to add a new one
 */
function mcms_form($form, &$form_state, $mlid = 'add') {
  $new = TRUE;
  if (0+$mlid > 0) {
    $mcms_data = _menu_multicol_load($mlid);
    if (is_object($mcms_data)) {
      $new = FALSE;
    }
  }

  $form['mcms'] = array(
    '#type' => 'fieldset',
    '#title' => $new ? 'Add New MultiCol Menu' : 'Edit MultiCol Menu: ' . $mcms_data->name,
    '#description' => t('Generate a multi column menu structure.'),
  );

  $form['mcms']['name'] = array(
    '#type' => 'textfield',
    '#title' => 'Name',
    '#description' => t('The name for this menu structure'),
    '#required' => TRUE,
  );
  if (! $new) {
    $form['mcms']['name']['#default_value'] = $mcms_data->name;
  }

  $form['mcms']['nodetype'] = array(
    '#type' => 'fieldset',
    '#title' => 'Node Type',
    '#description' => t('Create Menu for these Nodes.'),
  );
  $result = db_query('SELECT type, COUNT(nid) as tcount FROM {node} GROUP BY type ORDER BY type');
  $node_types = array();
  foreach ($result as $row) {
    $node_types[$row->type] = $row->type . ' (' . $row->tcount . ' Nodes)';
    if (!isset($nt_default)) $nt_default = $row->type;
  } // foreach
  $form['mcms']['nodetype']['node_type'] = array(
    '#type' => 'select',
    '#title' => 'Nodetype',
    '#description' => t('Generate menu structure for nodes of this type.'),
    '#options' => $node_types,
    '#default_value' => $new ? $nt_default : $mcms_data->type,
    '#required' => TRUE,
  );

  $form['mcms']['menu'] = array(
    '#type' => 'fieldset',
    '#title' => 'Menu Generation',
    '#description' => t('Menu Structure settings.'),
  );

  $result = db_query("SELECT mlid, menu_name, link_title FROM {menu_links} WHERE plid=0 and hidden=0 ORDER BY menu_name");
  $menus = array();
  $menu_names = array();
  foreach ($result as $row) {
    $menus[$row->menu_name][] = $row->link_title;
    if (! isset($menu_names[$row->menu_name])) {
      $menu_names[$row->menu_name] = $row->menu_name;
    }
    if (! $new) {
      // get default values for menu name and item, if editing existing data
      if ($row->mlid == $mlid) {
        $menu_name_default = $row->menu_name;
        $menu_item_default = $row->link_title;
      }
    }
  }
  $form['mcms']['menu']['menu_name'] = array(
    '#type' => 'select',
    '#title' => 'Menue Name',
    '#description' => t('Name of the parent menue in which the structure should be placed.'),
    '#options' => $menu_names,
    '#required' => TRUE,
  );
  if (isset($menu_name_default)) {
    $form['mcms']['menu']['menu_name']['#default_value'] = $menu_name_default;
  }

  foreach ($menu_names as $menu_name) {
    $form['mcms']['menu']['menu_item'][$menu_name] = array(
      '#type' => 'select',
      '#title' => 'Menue Item',
      '#description' => t('Name of the menu item under wich the columns will be created.'),
      '#options' => drupal_map_assoc($menus[$menu_name]),
      '#states' => array(
        'visible' => array(
          ':input[name="menu_name"]' => array('value' => $menu_name),
        ),
      ),
    );
    if (isset($menu_item_default) && ($menu_name == $menu_name_default)) {
      $form['mcms']['menu']['menu_item'][$menu_name]['#default_value'] = $menu_item_default;
    }
  }
  $form['mcms']['menu']['colprefix'] = array(
    '#type' => 'textfield',
    '#title' => 'Column Prefix',
    '#description' => t('Prefix for the names of the column sub-menues.'),
    '#default_value' => $new ? 'mcms_col_' : $mcms_data->colprefix,
    '#required' => TRUE,
  );
  $form['mcms']['menu']['colcount'] = array(
    '#type' => 'textfield',
    '#title' => 'Number of Columns',
    '#description' => t('The available nodes will be sorted in this count of columns.'),
    '#default_value' => $new ? 3 : $mcms_data->colcount,
    '#required' => TRUE,
  );
  $form['mcms']['menu']['colsort'] = array(
    '#type' => 'radios',
    '#title' => 'Sort Order',
    '#description' => t('Should the node titles be sorted horizontal (in rows), or vertical (in columns).'),
    '#options' => array(
      0 => t('horizontal'),
      1 => t('vertical')
    ),
    '#default_value' => $new ? 0 : $mcms_data->colsort,
  );
  $form['mcms']['menu']['link_title'] = array(
    '#type' => 'textfield',
    '#title' => 'Link Title',
    '#description' => t('Add a title attribute to each menu link.') . '<br />' .
      t('Use <em>@title</em> as a placeholder for the node title, e.g. <em>Informations related to @title</em>.'),
    '#default_value' => $new ? '' : $mcms_data->linktitle,
  );

  $form['mcms']['auto_update'] = array(
    '#type' => 'checkbox',
    '#title' => 'Auto-Upate',
    '#description' => t('Rebuild the menu structure every time nodes are modified.'),
    '#default_value' => $new ? FALSE : $mcms_data->autoupdate,
  );

  $form['mcms']['save'] = array(
    '#type' => 'submit',
    '#value' => $new ? t('Save & Generate Menu Structure') : t('Save & Update Menu Structure'),
    '#submit' => array('mcms_form_submit'),
  );
  $form['mcms']['clear'] = array(
    '#type' => 'submit',
    '#value' => t('Delete Menu Structure'),
    '#submit' => array('mcms_form_submit'),
    '#enabled' => !$new,
  );
  $form['mcms']['remarks'] = array(
    '#type' => 'item',
    '#title' => 'Remarks',
    '#description' => t('Settings are always saved, no matter wich button is clicked.') . '<br/>' .
      t('CAUTION: Pressing "Delete Menu Structure" will delete all menu items beneath the selected menu item!'),
  );
  return $form;
} // mcms_form()

/**
 * submit function to generate and maintain a menu structure for a specific
 *  node type, useable by Mega-Dropdown Menu
 */
function mcms_form_submit($form, &$form_state, $mlid = 'add') {
  $mcms_name = $form_state['values']['name'];
  $node_type = $form_state['values']['node_type'];
  $menu_name = $form_state['values']['menu_name'];
  $menu_item = $form_state['values'][$menu_name];
  $colprefix = trim($form_state['values']['colprefix']);
  $colcount = 0+$form_state['values']['colcount'];
  $colsort = $form_state['values']['colsort'];
  $link_title = trim($form_state['values']['link_title']);

  $plid = 0+db_query('SELECT mlid FROM {menu_links} WHERE menu_name = :menu_name AND link_title = :menu_item',
    array(':menu_name' => $menu_name, ':menu_item' => $menu_item))->fetchField();
  if (0 === $plid) {
    drupal_set_message(t('Menu Link ID for Menu Item %menu_item in Menu %menu_name not found!',
      array('%menu_item' => $menu_item, '%menu_name' => $menu_name)), 'error');
    return FALSE;
  }

  if (t('Delete Menu Structure') == $form_state['values']['op']) {
    if (_menu_multicol_delete($plid)) {
      drupal_set_message(t('The whole MultiColumn Menu Structure has been cleared.'));
      // Delete the dataset
      db_delete('menu_multicol')
      ->condition('mlid', $plid)
      ->execute();
      // Go back to the list page
      drupal_goto('admin/structure/menu/multicol');
    }
    else {
      drupal_set_message(t('Error deleting the menu structure.'), 'error');
      return FALSE;
    }
  }

  // create / update dataset with parameters
  db_merge('menu_multicol')
    ->key(array('mlid' => $plid))
    ->fields(array(
      'name' => $mcms_name,
      'colcount' => $colcount,
      'colprefix' => $colprefix,
      'colsort' => $colsort,
      'type' => $node_type,
      'linktitle' => $link_title,
      'autoupdate' => $form_state['values']['auto_update'],
    ))
    ->execute();

  // get the column menus
  $colmenus = _menu_multicol_get_colmenus($plid);
  if (count($colmenus) < $colcount) {
    // create / edit missing column submenus
    for ($i = 1; $i <= $colcount; $i++) {
      $colmenu = $colprefix . $i;
      if (isset($colmenus[$i])) {
        if ($colmenu === $colmenus[$i]['link_title']) {
          // with correct naming, nothing to do here...
          continue;
        }
        else {
          // just rename it
          $colmenus[$i]['link_title'] = $colmenu;
        }
      }
      else {
        // create column sub-menu
        $colmenus[$i] = array(
          'link_path' => '<nolink>',
          'link_title' => $colmenu,
          'menu_name' => $menu_name,
          'expanded' => TRUE,
          'plid' => $plid,
        );
      }
      $mlid = menu_link_save($colmenus[$i]);
      if (FALSE === $mlid) {
        drupal_set_message(t('Coul not create column menu "%colmenu".', array('%colmenu' => $colmenu)), 'error');
      }
      else {
        drupal_set_message(t('Column menu "%lt" has been saved.', array('%lt' => $colmenu)));
        // this is not needed, because menu_link_save has done this already for us...
        // $colmenus[$i]['mlid'] = $mlid;
      }
    } // for
  }


  menu_cache_clear($menu_name);

  _menu_multicol_update_links($plid);

  // get rid of submenus not needed any more
  $i = count($colmenus);
  while ($i > $colcount) {
    menu_link_delete($colmenus[$i]['mlid']);
    drupal_set_message(t('Column submenu "%lt" deleted, because it is no longer used.',
      array('%lt' => $colmenus[$i]['link_title'])));
    unset($colmenus[$i]);
    $i--;
  }

  menu_cache_clear($menu_name);

   // Go back to the list page
  drupal_goto('admin/structure/menu/multicol');
} // mcms_form_submit()

/**
 * Get the parent menu id for the column submenus
 * @return int plid
 */
function _menu_multicol_get_plid() {
  $plid = 0;

  $menu_name = variable_get('menu_multicol_menu_name', '');
  if ('' == $menu_name) return $plid;

  $menu_item = variable_get('menu_multicol_menu_item', '');
  if ('' == $menu_item) return $plid;

  $menu_main = menu_load_links($menu_name);

  // get parent menu id for column submenus
  foreach ($menu_main as $submenu) {
    $ltitle = $submenu['link_title'];
    if ($menu_item === $ltitle) {
      return $submenu['mlid'];
    }
  }
  drupal_set_message(t('Parent menu entry "%menu_item" does not exist.', array('%menu_item' => $menu_item)), 'error');

  return $plid;
}

/**
 * Get the column menu items as array
 *
 * @param int plid parent link id
 * @return array of colmenu objects
 */
function _menu_multicol_get_colmenus($plid) {
  $colmenus = array();
  if (0 == $plid) return $colmenus;

  // get existing column submenus
  $result = db_select('menu_links', 'ml')
    ->fields('ml', array('mlid', 'link_title', 'has_children', 'weight'))
    ->condition('plid', $plid)
    ->condition('link_path', '<nolink>')
    ->execute();
  $i = 1;
  foreach ($result as $row) {
    $colmenus[$i++] = get_object_vars($row);
  }

  return $colmenus;
} // _menu_multicol_get_colmenus()

/**
 * Helper function for deleting the multi column menu structure
 */
function _menu_multicol_delete($mlid = 0, $recdepth = 0) {
    // Delete whole menu structure beneath the given menu item
    if (0 == $mlid) {
      drupal_set_message(t('Deletion of all menus is not allowed!', 'error'));
      return FALSE;
    }
    if ($recdepth++ > 10) {
      drupal_set_message(t('Maximum recursion depth reached!'), 'error');
      return FALSE;
    }
    $result = db_select('menu_links', 'ml')
      ->fields('ml', array('mlid', 'menu_name', 'link_title', 'has_children'))
      ->condition('plid', $mlid)
      ->execute();
    foreach ($result as $row) {
      if ($row->has_children) {
        // CAUTION: Recursive deletion of submenus
        drupal_set_message(t('Deleting submenu %lt...', array('%lt' => $row->link_title)));
        if (FALSE == _menu_multicol_delete($row->mlid, $recdepth)) return FALSE;
      }
      drupal_set_message(t('Deleting menu link %lt...', array('%lt' => $row->link_title)));
      menu_link_delete($row->mlid);
    }

    return TRUE;
} // _menu_multicol_delete()

/**
 * Helper function for creating / updating the menu-links in the bottom layer
 *
 * @param int mlid parent menu link id
 */
function _menu_multicol_update_links($mlid) {
  // clear pending cron updates
  $updates = variable_get('menu_multicol_update_required', array());
  if (in_array($mlid, $updates)) {
    $updates = array_diff($updates, array($mlid));
    variable_set('menu_multicol_update_required', $updates);
  }

  // get dataset for the requested menu structure
  $mcms_data = _menu_multicol_load($mlid);
  if (FALSE === $mcms_data) return FALSE;

  // get an array of column submenu plids
  $colmenus = _menu_multicol_get_colmenus($mlid);
  if (count($colmenus) < 0+$mcms_data->colcount) {
    watchdog('Menu MultiCol', 'Missing submenu beneath mlid %mlid. AutoUpdate disabled for %name.',
      array('%mlid' => $mlid, '%name' => $mcms_data->name), WATCHDOG_ERROR);
    db_merge('menu_multicol')
      ->key(array('mlid' => $mlid))
      ->fields(array('autoupdate' => FALSE))
      ->execute();
    return FALSE;
  }

  $col_plids = array();
  foreach ($colmenus as $menu_item) {
    $col_plids[] = $menu_item['mlid'];
  }

  // get an array of existing menu-links which we can reuse
  $menu_name = db_query('SELECT menu_name FROM {menu_links} WHERE mlid = :mlid', array(':mlid' => $mlid))->fetchField();
  $menu_main = menu_load_links($menu_name);
  $items = array();
  foreach ($menu_main as $menu_item) {
    // no Layer1 entries
    if (!in_array($menu_item['plid'], $col_plids)) continue;

    // search for layer 3 entries and key with node-id
    if ('node/' === drupal_substr($menu_item['link_path'], 0, 5)) {
      $nid = 0+drupal_substr($menu_item['link_path'], 5);
      $items[$nid] = $menu_item['mlid'];
    }
  }

  // get all nodes of requested type
  $query = db_select('node', 'n')
    ->fields('n', array('nid', 'title'))
    ->condition('type', $mcms_data->type)
    ->condition('status', 1)
    ->orderBy('title');
  $item_count = $query->countQuery()->execute()->fetchField();
  $colcount = $mcms_data->colcount;
  $colsort = (bool)$mcms_data->colsort;
  $maxitemspercol = ceil($item_count/$colcount);

  $result = $query->orderBy('title')->execute();
  $col_index = 1;
  $link_count = 0;
  foreach ($result as $row) {
    $item = array(
      'link_path' => 'node/' . $row->nid,
      'link_title' => $row->title,
      'menu_name' => $menu_name,
      'expanded' => TRUE,
      'plid'  => $colmenus[$col_index]['mlid'],
    );
    if ('' != $mcms_data->linktitle) {
      $item['options']['attributes']['title'] = format_string($mcms_data->linktitle, array('@title' => $row->title));
    }
    // set mlid for existing menu items, so they get just updated
    if (isset($items[$row->nid])) {
      $item['mlid'] = $items[$row->nid];
      // and reset weight
      $item['weight'] = 0;
      unset($items[$row->nid]);
    }
    if (FALSE === menu_link_save($item)) {
      watchdog('Menu MultiCol', 'Menu entry for node "%nt" could not be saved.',
        array('%nt' => $row->title), WATCHDOG_WARNING);
    }
    else {
      drupal_set_message(t('Menu entry for node "%nt" has been placed in column submenu "%ct".',
        array('%nt' => $row->title, '%ct' => $colmenus[$col_index]['link_title'])));
    }
    if ($colsort) {
      // sort vertical (in columns)
      $link_count++;
      if ($link_count >= $maxitemspercol) {
        $item_count -= $link_count;
        $link_count = 0;
        $colcount--;
        if ($colcount > 0) {
          $col_index++;
          $maxitemspercol = ceil($item_count / $colcount);
        } // colcount
      } // link_count
    }
    else {
      $col_index++;
      if ($col_index > $colcount) $col_index = 1;
    } // colsort horizontal (in lines)
  } // foreach

  // if nodes were deleted, there will be stale entries left over in items
  foreach ($items as $nid => $mlid) {
    menu_link_delete($mlid);
    watchdog('Menu MultiCol', 'Menu-link for node %nid deleted, because the node has been deleted.',
      array('%nid' => $nid));
  }
  // clear the menu cache, since we modified the menu structure
  menu_cache_clear_all();
  menu_rebuild();

  return TRUE;
} // _menu_multicol_update_links

/**
 * Helper function to load menu structure data from database
 *
 * @param int $mlid menu link id (primary key in menu_multicol table)
 * @return
 */
function _menu_multicol_load($mlid) {
  return db_select('menu_multicol', 'mmc')
    ->fields('mmc')
    ->condition('mlid', $mlid)
    ->range(0, 1)
    ->execute()
    ->fetchObject();
}